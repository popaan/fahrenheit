package sheridan;

public class Fahrenheit {
	public static int fromCelcius(int celcius) {
		return Math.round((celcius * 9 / 5) + 32);
	}
}
