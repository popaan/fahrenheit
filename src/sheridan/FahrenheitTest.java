package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

/*
 * @author Andreea Popa - 991519111 
 */

class FahrenheitTest {

	@Test
	void testFromCelciusRegular() {
		int fahrenheit = Fahrenheit.fromCelcius(10);
		
		assertTrue("Incorrect output", fahrenheit == 50);
	}
	
	@Test
	void testFromCelciusBoundaryInPositiveNumber() {
		int fahrenheit = Fahrenheit.fromCelcius(1);
				
		assertTrue("Incorrect output", fahrenheit == 33);
	}
	
	@Test
	void testFromCelciusBoundaryInNegativeNumber() {
		int fahrenheit = Fahrenheit.fromCelcius(-1);
						
		assertTrue("Incorrect output", fahrenheit == 31);
	}
	
	@Test
	void testFromCelciusBoundaryOutPositiveNumber() {
		int fahrenheit = Fahrenheit.fromCelcius(0);
				
		assertTrue("Incorrect output", fahrenheit == 32);
	}
	
	@Test
	void testFromCelciusBoundaryOutNegativeNumber() {
		int fahrenheit = Fahrenheit.fromCelcius(-18);
								
		assertTrue("Incorrect output", fahrenheit == 0);
	}
	
	@Test
	void testFromCelciusException() {
		int fahrenheit = Fahrenheit.fromCelcius(19);
		
		assertTrue("Incorrect output", fahrenheit == 66);
	}

}
